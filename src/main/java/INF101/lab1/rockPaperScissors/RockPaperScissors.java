package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;


public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> continuePlayingChoices = Arrays.asList("y", "n", "yes", "no");
    
    public void run() {
        System.out.println("Let's play round " + roundCounter);
        // Menneske- og computervalg
        String humanChoice = userChoice();
        String computerChoice = randomChoice(rpsChoices);
        String choiceString = ("Human chose " +humanChoice+ ", computer chose " + computerChoice + "."); 

        // Sjekker hvem som vant
        if (isWinner(humanChoice, computerChoice) == true) {
            System.out.println(choiceString + " Human wins.");
            humanScore++;
        }
        else if (isWinner(computerChoice, humanChoice) == true) {
            System.out.println(choiceString + " Computer wins.");
            computerScore++;
        }
        else {
            System.out.println(choiceString + " It's a tie");
        }
        System.out.println("Score: human: " + humanScore + ", computer: " + computerScore);
        roundCounter++;
        
        // Spørre om brukeren vil spille igjen
        String continueAnswer = continuePlaying();
        while (true) {
            if ("y".equalsIgnoreCase(continueAnswer) || "yes".equalsIgnoreCase(continueAnswer)) {
                run();
            }
            else {
                System.out.println("Bye bye :)");
                break;
                
            }
        }
    }

    // Lager en metode for at computeren velger tilfeldig mellom stein, saks, eller papir.
    public static String randomChoice(List<String> rpsChoices) {
        Random random = new Random();
        int randomIndex = random.nextInt(rpsChoices.size());
        
        return rpsChoices.get(randomIndex);
    }
    
    // Lager en metode for å sjekke vinneren
    public static Boolean isWinner(String choice1, String choice2) {

        if (choice1.equals("paper") && choice2.equals("rock")) {
            return true;
        }
        else if (choice1.equals("scissors") && choice2.equals("paper")) {
            return true;
        }
        else if (choice1.equals("rock") && choice2.equals("scissors")) {
            return true;
        }
        else return false;
    }

    // Lager en metode for å validere bruker-input
    public String userChoice() {
        String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
        while (true) {
            if (validResponse(humanChoice)) {
                return humanChoice;
            }
            else {
                System.out.println("I don't understand " +humanChoice+". " + "Try again.");
                return userChoice();

            }

        }
        
    }
    // Lager en metode for å validere bruker-input
    public static Boolean validResponse(String userChoice) {
        return userChoice.equals("rock") || userChoice.equals("paper") || userChoice.equals("scissors");
    }

    // Lager en metode for å spørre brukeren om den vil fortsette spillet
    public String continuePlaying() {
        while (true) {
            String continueAnswer = readInput("Continue playing? (y/n)").toLowerCase();
            if (continuePlaying(continueAnswer, continuePlayingChoices)) {
                return continueAnswer;
            }
            else System.out.println("I don't understand " +continueAnswer+". " + "Try again.");

        }
    }

    public Boolean continuePlaying(String input, List<String> validInput) {
        input = input.toLowerCase();
        return validInput.contains(input);
    }
    
    

    /**
     * Reads input from console with given prompt
     * @param prompt 
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}