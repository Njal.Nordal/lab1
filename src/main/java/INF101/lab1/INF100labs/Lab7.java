package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        for (int i = 0; i < grid.size(); i++) {
            if (i == row) {
                grid.remove(i);
            }
        }
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        
        int numRows = grid.size();
        int numCols = grid.get(0).size();

        // Finn sum av første kolonne
        int referenceSum = 0;
        for (int j = 0; j < numCols; j++) {
            referenceSum += grid.get(0).get(j);
        }

        // Sjekker summen av rader
        for (int i = 1; i < numRows; i++) {
            int rowSum = 0;
            for (int j = 0; j < numCols; j++) {
                rowSum += grid.get(i).get(j);
            }
            if (rowSum != referenceSum) {
                return false;
            }
        }

        // Sjekker summen av kollonner
        for (int j = 0; j < numCols; j++) {
            int colSum = 0;
            for (int i = 0; i < numRows; i++) {
                colSum += grid.get(i).get(j);
            }
            if (colSum != referenceSum) {
                return false;
            }
        }

        return true;
    }
}

