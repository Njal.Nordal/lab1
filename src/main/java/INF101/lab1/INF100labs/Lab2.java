package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int lenWord1 = word1.length();
        int lenWord2 = word2.length();
        int lenWord3 = word3.length();

        if (lenWord2 > lenWord1 && lenWord2 > lenWord3) {
            System.out.println(word2);
        }
        else if (lenWord3 > lenWord1 && lenWord3 > lenWord2) {
            System.out.println(word3);
        }
        else if (lenWord1 == lenWord2 && lenWord1 == lenWord3) {
            System.out.println(word1);
            System.out.println(word2);
            System.out.println(word3);
        }
        else if (lenWord1 == lenWord2) {
            System.out.println(word1);
            System.out.println(word2);
        }
        else if (lenWord2 == lenWord3) {
            System.out.println(word2);
            System.out.println(word3);
        }
        else if (lenWord3 == lenWord1) {
            System.out.println(word1);
            System.out.println(word3);
        }
        else System.out.println(word1);
        }

    public static boolean isLeapYear(int year) {
     if (year % 4 == 0 && year % 100 == 0 && year % 400 == 0) {
        return true;
     }
     else if (year % 4 == 0 && year % 100 == 0) {
        return false;
     }
     else if (year % 4 == 0) {
        return true;
     }
     else return false;
        
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num < 0) {
            return false;
        }
        else if (num % 2 != 0) {
            return false;
        }
        else return true;
    }

}
